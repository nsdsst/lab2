# app.py
from flask import Flask, jsonify, request
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'mysql-service'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '6KvSx6NDvRvKeDm0'
app.config['MYSQL_DB'] = 'todo_db'

mysql = MySQL(app)

@app.route('/tasks', methods=['GET'])
def get_tasks():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM tasks')
    tasks = cur.fetchall()
    cur.close()
    return jsonify(tasks)

@app.route('/tasks', methods=['POST'])
def add_task():
    task = request.get_json()
    cur = mysql.connection.cursor()
    cur.execute('INSERT INTO tasks (title, description) VALUES (%s, %s)', (task['title'], task['description']))
    mysql.connection.commit()
    cur.close()
    return jsonify({'message': 'Task added successfully'})

@app.route('/tasks/<int:task_id>', methods=['PUT'])
def update_task(task_id):
    task = request.get_json()
    cur = mysql.connection.cursor()
    cur.execute('UPDATE tasks SET title=%s, description=%s WHERE id=%s', (task['title'], task['description'], task_id))
    mysql.connection.commit()
    cur.close()
    return jsonify({'message': 'Task updated successfully'})

@app.route('/tasks/<int:task_id>', methods=['DELETE'])
def delete_task(task_id):
    cur = mysql.connection.cursor()
    cur.execute('DELETE FROM tasks WHERE id=%s', (task_id,))
    mysql.connection.commit()
    cur.close()
    return jsonify({'message': 'Task deleted successfully'})

if __name__ == '__main__':
    app.run(debug=True)
